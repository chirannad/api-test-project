<?php

namespace Tests\Feature\AutoGen\API\V1;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class BooksShowBookAPITest extends APIBaseTestCase
{

    use DatabaseTransactions;

    /**
     *
     * 
     *
     * @return  void
     */
    public function test_api_books_get_show_book()
    {
        $data = $cookies = $files = $headers = $server = [];
        $faker = \Faker\Factory::create('en_AU');
        $content = null;

        // header params
        $headers['Accept'] = 'application/json';
        $headers['x-access-token'] = $this->getAccessToken();
        $headers['x-api-key'] = $this->getApiKey();


        $response = $this->get('/api/v1/books/{id}', $headers);

        $this->saveResponse($response->getContent(), 'books_get_show_book', $response->getStatusCode());

        $response->assertStatus(200);
    }
}
