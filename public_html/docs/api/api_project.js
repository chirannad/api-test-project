define({
  "name": "WebApp Backend API",
  "version": "1.0.0",
  "description": "Web service to communicate with the backend.",
  "url": "",
  "sampleUrl": false,
  "defaultVersion": "0.0.0",
  "apidoc": "0.3.0",
  "generator": {
    "name": "apidoc",
    "time": "2021-08-02T08:26:14.553Z",
    "url": "https://apidocjs.com",
    "version": "0.28.1"
  }
});
