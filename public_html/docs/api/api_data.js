define({ "api": [
  {
    "description": "<p>Logout the user from current device</p>",
    "version": "1.0.0",
    "type": "GET",
    "url": "api/v1/logout",
    "title": "Logout",
    "group": "Auth",
    "filename": "/home/chiran/Documents/Emedia/chiran/projects/testApiProject/resources/docs/apidoc/auto_generated/auth.coffee",
    "groupTitle": "Auth",
    "name": "GetApiV1Logout",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Accept",
            "description": "<p>Set to <code>application/json</code></p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "x-api-key",
            "description": "<p>API Key</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>Unique user authentication token</p>"
          }
        ]
      }
    }
  },
  {
    "version": "1.0.0",
    "type": "POST",
    "url": "api/v1/login",
    "title": "Login",
    "group": "Auth",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "device_id",
            "description": "<p>Unique ID of the device</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "device_type",
            "description": "<p>Type of the device <code>APPLE</code> or <code>ANDROID</code></p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "device_push_token",
            "description": "<p>Unique push token for the device</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Email</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>Password</p>"
          }
        ]
      }
    },
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Accept",
            "description": "<p><code>application/json</code></p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "x-api-key",
            "description": "<p>API Key</p>"
          }
        ]
      }
    },
    "filename": "/home/chiran/Documents/Emedia/chiran/projects/testApiProject/resources/docs/apidoc/auto_generated/auth.coffee",
    "groupTitle": "Auth",
    "name": "PostApiV1Login"
  },
  {
    "version": "1.0.0",
    "type": "POST",
    "url": "api/v1/password/edit",
    "title": "Update Password",
    "group": "Auth",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>Password</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "current_password",
            "description": "<p>Current password</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password_confirmation",
            "description": "<p>Password confirmation</p>"
          }
        ]
      }
    },
    "filename": "/home/chiran/Documents/Emedia/chiran/projects/testApiProject/resources/docs/apidoc/auto_generated/auth.coffee",
    "groupTitle": "Auth",
    "name": "PostApiV1PasswordEdit",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Accept",
            "description": "<p>Set to <code>application/json</code></p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "x-api-key",
            "description": "<p>API Key</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>Unique user authentication token</p>"
          }
        ]
      }
    }
  },
  {
    "description": "<p>This endpoint registers a user.If you need to update a profile image, upload the profile image in thebackground using <code>/avatar</code> endpoint.</p>",
    "version": "1.0.0",
    "type": "POST",
    "url": "api/v1/register",
    "title": "Register",
    "group": "Auth",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "device_id",
            "description": "<p>Unique ID of the device</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "device_type",
            "description": "<p>Type of the device <code>APPLE</code> or <code>ANDROID</code></p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "device_push_token",
            "description": "<p>Unique push token for the device</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "first_name",
            "description": "<p>First name of user</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "last_name",
            "description": "<p>Last name of user</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Email address of user</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>Password. Must be at least 8 characters.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password_confirmation",
            "description": "<p>Confirm password. Must be at least 8 characters.</p>"
          }
        ]
      }
    },
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Accept",
            "description": "<p><code>application/json</code></p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "x-api-key",
            "description": "<p>API Key</p>"
          }
        ]
      }
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response / HTTP 422 Unprocessable Entity",
          "content": "{\n\"message\": \"The email must be a valid email address.\",\n\"payload\": {\n\"errors\": {\n\"email\": [\n\"The email must be a valid email address.\"\n]\n}\n},\n\"result\": false\n}",
          "type": "json"
        }
      ]
    },
    "filename": "/home/chiran/Documents/Emedia/chiran/projects/testApiProject/resources/docs/apidoc/auto_generated/auth.coffee",
    "groupTitle": "Auth",
    "name": "PostApiV1Register"
  },
  {
    "version": "1.0.0",
    "type": "GET",
    "url": "api/v1/books",
    "title": "Get Books",
    "group": "Books",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "q",
            "description": "<p>Name of the Book</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "page",
            "description": "<p>Page number</p>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": true,
            "field": "per_page",
            "description": "<p>Number of items per page</p>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": true,
            "field": "sort_by_date",
            "description": "<p>new to old =&gt; 1 / old to new =&gt; 2</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response / HTTP 200 OK",
          "content": "{\n\"payload\": [\n{\n\"id\": 50,\n\"book_name\": \"Oceane Kemmer\",\n\"author_name\": \"Prof. Sally Johns IV\",\n\"isbn_no\": \"9794800151673\",\n\"published_year\": 2016,\n\"created_at\": \"2021-08-02T07:13:38.000000Z\",\n\"updated_at\": \"2021-08-02T07:13:38.000000Z\"\n},\n{\n\"id\": 49,\n\"book_name\": \"Vidal Lemke\",\n\"author_name\": \"Dr. Jerald Sauer II\",\n\"isbn_no\": \"9790566260017\",\n\"published_year\": 2005,\n\"created_at\": \"2021-08-02T07:13:38.000000Z\",\n\"updated_at\": \"2021-08-02T07:13:38.000000Z\"\n},\n{\n\"id\": 48,\n\"book_name\": \"Miss Tania McGlynn\",\n\"author_name\": \"Samir Lind DVM\",\n\"isbn_no\": \"9796263229654\",\n\"published_year\": 1986,\n\"created_at\": \"2021-08-02T07:13:38.000000Z\",\n\"updated_at\": \"2021-08-02T07:13:38.000000Z\"\n},\n{\n\"id\": 47,\n\"book_name\": \"Mr. Lemuel Littel Sr.\",\n\"author_name\": \"Casandra Hahn\",\n\"isbn_no\": \"9781983580420\",\n\"published_year\": 1991,\n\"created_at\": \"2021-08-02T07:13:38.000000Z\",\n\"updated_at\": \"2021-08-02T07:13:38.000000Z\"\n},\n{\n\"id\": 46,\n\"book_name\": \"Jordy Brown\",\n\"author_name\": \"Miss Ashly Mante DDS\",\n\"isbn_no\": \"9791704481363\",\n\"published_year\": 1983,\n\"created_at\": \"2021-08-02T07:13:38.000000Z\",\n\"updated_at\": \"2021-08-02T07:13:38.000000Z\"\n},\n{\n\"id\": 45,\n\"book_name\": \"Ottis Spencer\",\n\"author_name\": \"Miss Ashlee Conroy PhD\",\n\"isbn_no\": \"9787354326437\",\n\"published_year\": 2014,\n\"created_at\": \"2021-08-02T07:13:38.000000Z\",\n\"updated_at\": \"2021-08-02T07:13:38.000000Z\"\n},\n{\n\"id\": 44,\n\"book_name\": \"Mr. Khalid Morissette\",\n\"author_name\": \"Camila Sauer\",\n\"isbn_no\": \"9790847425654\",\n\"published_year\": 2014,\n\"created_at\": \"2021-08-02T07:13:38.000000Z\",\n\"updated_at\": \"2021-08-02T07:13:38.000000Z\"\n},\n{\n\"id\": 43,\n\"book_name\": \"Armani Jenkins\",\n\"author_name\": \"Jaime Tillman DVM\",\n\"isbn_no\": \"9797143938918\",\n\"published_year\": 2021,\n\"created_at\": \"2021-08-02T07:13:38.000000Z\",\n\"updated_at\": \"2021-08-02T07:13:38.000000Z\"\n},\n{\n\"id\": 42,\n\"book_name\": \"Kristoffer Trantow\",\n\"author_name\": \"Bertha Walter\",\n\"isbn_no\": \"9795528980781\",\n\"published_year\": 1996,\n\"created_at\": \"2021-08-02T07:13:38.000000Z\",\n\"updated_at\": \"2021-08-02T07:13:38.000000Z\"\n},\n{\n\"id\": 41,\n\"book_name\": \"Amya Hill Sr.\",\n\"author_name\": \"Prof. Nikko Hessel\",\n\"isbn_no\": \"9798774983186\",\n\"published_year\": 1990,\n\"created_at\": \"2021-08-02T07:13:38.000000Z\",\n\"updated_at\": \"2021-08-02T07:13:38.000000Z\"\n},\n{\n\"id\": 40,\n\"book_name\": \"Helmer Miller I\",\n\"author_name\": \"Mrs. Maudie Collier\",\n\"isbn_no\": \"9794350915527\",\n\"published_year\": 1986,\n\"created_at\": \"2021-08-02T07:13:38.000000Z\",\n\"updated_at\": \"2021-08-02T07:13:38.000000Z\"\n},\n{\n\"id\": 39,\n\"book_name\": \"Mr. Tillman Hartmann II\",\n\"author_name\": \"Clara Paucek\",\n\"isbn_no\": \"9788312021739\",\n\"published_year\": 1978,\n\"created_at\": \"2021-08-02T07:13:38.000000Z\",\n\"updated_at\": \"2021-08-02T07:13:38.000000Z\"\n},\n{\n\"id\": 38,\n\"book_name\": \"Mrs. Ocie Labadie IV\",\n\"author_name\": \"Marisa Rowe DDS\",\n\"isbn_no\": \"9791281869332\",\n\"published_year\": 2004,\n\"created_at\": \"2021-08-02T07:13:38.000000Z\",\n\"updated_at\": \"2021-08-02T07:13:38.000000Z\"\n},\n{\n\"id\": 37,\n\"book_name\": \"Dr. Marjorie Eichmann\",\n\"author_name\": \"Hulda Wolff\",\n\"isbn_no\": \"9797411542335\",\n\"published_year\": 1979,\n\"created_at\": \"2021-08-02T07:13:38.000000Z\",\n\"updated_at\": \"2021-08-02T07:13:38.000000Z\"\n},\n{\n\"id\": 36,\n\"book_name\": \"Miss Melisa Douglas\",\n\"author_name\": \"Kaela Parker PhD\",\n\"isbn_no\": \"9792173791618\",\n\"published_year\": 1973,\n\"created_at\": \"2021-08-02T07:13:38.000000Z\",\n\"updated_at\": \"2021-08-02T07:13:38.000000Z\"\n},\n{\n\"id\": 35,\n\"book_name\": \"Dr. Dorothy Jerde DVM\",\n\"author_name\": \"Ms. Bryana Watsica\",\n\"isbn_no\": \"9786724250624\",\n\"published_year\": 1992,\n\"created_at\": \"2021-08-02T07:13:38.000000Z\",\n\"updated_at\": \"2021-08-02T07:13:38.000000Z\"\n},\n{\n\"id\": 34,\n\"book_name\": \"Jared Paucek\",\n\"author_name\": \"Mrs. Monica Grant\",\n\"isbn_no\": \"9796837851472\",\n\"published_year\": 1985,\n\"created_at\": \"2021-08-02T07:13:38.000000Z\",\n\"updated_at\": \"2021-08-02T07:13:38.000000Z\"\n},\n{\n\"id\": 33,\n\"book_name\": \"Lucinda Barrows\",\n\"author_name\": \"Estelle Beier\",\n\"isbn_no\": \"9793799869064\",\n\"published_year\": 1986,\n\"created_at\": \"2021-08-02T07:13:38.000000Z\",\n\"updated_at\": \"2021-08-02T07:13:38.000000Z\"\n},\n{\n\"id\": 32,\n\"book_name\": \"Maynard Lubowitz\",\n\"author_name\": \"Una Greenfelder\",\n\"isbn_no\": \"9788736166191\",\n\"published_year\": 1986,\n\"created_at\": \"2021-08-02T07:13:38.000000Z\",\n\"updated_at\": \"2021-08-02T07:13:38.000000Z\"\n},\n{\n\"id\": 31,\n\"book_name\": \"Prof. Orrin Oberbrunner\",\n\"author_name\": \"Albert Tremblay\",\n\"isbn_no\": \"9783815197080\",\n\"published_year\": 1976,\n\"created_at\": \"2021-08-02T07:13:38.000000Z\",\n\"updated_at\": \"2021-08-02T07:13:38.000000Z\"\n}\n],\n\"paginator\": {\n\"current_page\": 1,\n\"first_page_url\": \"http://127.0.0.1:8000/api/v1/books?page=1\",\n\"from\": 1,\n\"last_page\": 3,\n\"last_page_url\": \"http://127.0.0.1:8000/api/v1/books?page=3\",\n\"links\": [\n{\n\"url\": null,\n\"label\": \"&laquo; Previous\",\n\"active\": false\n},\n{\n\"url\": \"http://127.0.0.1:8000/api/v1/books?page=1\",\n\"label\": \"1\",\n\"active\": true\n},\n{\n\"url\": \"http://127.0.0.1:8000/api/v1/books?page=2\",\n\"label\": \"2\",\n\"active\": false\n},\n{\n\"url\": \"http://127.0.0.1:8000/api/v1/books?page=3\",\n\"label\": \"3\",\n\"active\": false\n},\n{\n\"url\": \"http://127.0.0.1:8000/api/v1/books?page=2\",\n\"label\": \"Next &raquo;\",\n\"active\": false\n}\n],\n\"next_page_url\": \"http://127.0.0.1:8000/api/v1/books?page=2\",\n\"path\": \"http://127.0.0.1:8000/api/v1/books\",\n\"per_page\": 20,\n\"prev_page_url\": null,\n\"to\": 20,\n\"total\": 50\n},\n\"message\": \"\",\n\"result\": true\n}",
          "type": "json"
        }
      ]
    },
    "filename": "/home/chiran/Documents/Emedia/chiran/projects/testApiProject/resources/docs/apidoc/auto_generated/books.coffee",
    "groupTitle": "Books",
    "name": "GetApiV1Books",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Accept",
            "description": "<p>Set to <code>application/json</code></p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "x-api-key",
            "description": "<p>API Key</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>Unique user authentication token</p>"
          }
        ]
      }
    }
  },
  {
    "version": "1.0.0",
    "type": "GET",
    "url": "api/v1/books/{id}",
    "title": "Show Book",
    "group": "Books",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "id",
            "description": "<p>ID of the book</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response / HTTP 200 OK",
          "content": "{\n\"payload\": {\n\"id\": 2,\n\"book_name\": \"Gloria Prosacco\",\n\"author_name\": \"Martine Skiles\",\n\"isbn_no\": \"9798874915537\",\n\"published_year\": 1979,\n\"created_at\": \"2021-08-02T07:13:38.000000Z\",\n\"updated_at\": \"2021-08-02T07:13:38.000000Z\"\n},\n\"message\": \"\",\n\"result\": true\n}",
          "type": "json"
        }
      ]
    },
    "filename": "/home/chiran/Documents/Emedia/chiran/projects/testApiProject/resources/docs/apidoc/auto_generated/books.coffee",
    "groupTitle": "Books",
    "name": "GetApiV1BooksId",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Accept",
            "description": "<p>Set to <code>application/json</code></p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "x-api-key",
            "description": "<p>API Key</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>Unique user authentication token</p>"
          }
        ]
      }
    }
  },
  {
    "version": "1.0.0",
    "type": "POST",
    "url": "api/v1/password/email",
    "title": "Reset Password",
    "group": "ForgotPassword",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Email</p>"
          }
        ]
      }
    },
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Accept",
            "description": "<p><code>application/json</code></p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "x-api-key",
            "description": "<p>API Key</p>"
          }
        ]
      }
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response / HTTP 422 Unprocessable Entity",
          "content": "{\n\"message\": \"Failed to send password reset email. Ensure your email is correct and try again.\",\n\"payload\": null,\n\"result\": false\n}",
          "type": "json"
        }
      ]
    },
    "filename": "/home/chiran/Documents/Emedia/chiran/projects/testApiProject/resources/docs/apidoc/auto_generated/forgot_password.coffee",
    "groupTitle": "ForgotPassword",
    "name": "PostApiV1PasswordEmail"
  },
  {
    "description": "<p>Guest settings and parameters</p>",
    "version": "1.0.0",
    "type": "GET",
    "url": "api/v1/guests",
    "title": "Guest Settings",
    "group": "Guest",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Accept",
            "description": "<p><code>application/json</code></p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "x-api-key",
            "description": "<p>API Key</p>"
          }
        ]
      }
    },
    "filename": "/home/chiran/Documents/Emedia/chiran/projects/testApiProject/resources/docs/apidoc/auto_generated/guest.coffee",
    "groupTitle": "Guest",
    "name": "GetApiV1Guests"
  },
  {
    "description": "<p>Get currently logged in user's profile</p>",
    "version": "1.0.0",
    "type": "GET",
    "url": "api/v1/profile",
    "title": "My Profile",
    "group": "Profile",
    "filename": "/home/chiran/Documents/Emedia/chiran/projects/testApiProject/resources/docs/apidoc/auto_generated/profile.coffee",
    "groupTitle": "Profile",
    "name": "GetApiV1Profile",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Accept",
            "description": "<p>Set to <code>application/json</code></p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "x-api-key",
            "description": "<p>API Key</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>Unique user authentication token</p>"
          }
        ]
      }
    }
  },
  {
    "version": "1.0.0",
    "type": "POST",
    "url": "api/v1/avatar",
    "title": "Update My Avatar",
    "group": "Profile",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "File",
            "optional": false,
            "field": "image",
            "description": "<p>Image</p>"
          }
        ]
      }
    },
    "filename": "/home/chiran/Documents/Emedia/chiran/projects/testApiProject/resources/docs/apidoc/auto_generated/profile.coffee",
    "groupTitle": "Profile",
    "name": "PostApiV1Avatar",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Accept",
            "description": "<p>Set to <code>application/json</code></p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "x-api-key",
            "description": "<p>API Key</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>Unique user authentication token</p>"
          }
        ]
      }
    }
  },
  {
    "version": "1.0.0",
    "type": "PUT",
    "url": "api/v1/profile",
    "title": "Update My Profile",
    "group": "Profile",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "first_name",
            "description": "<p>First name</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "last_name",
            "description": "<p>Last name</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Email</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "phone",
            "description": "<p>Phone</p>"
          }
        ]
      }
    },
    "filename": "/home/chiran/Documents/Emedia/chiran/projects/testApiProject/resources/docs/apidoc/auto_generated/profile.coffee",
    "groupTitle": "Profile",
    "name": "PutApiV1Profile",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Accept",
            "description": "<p>Set to <code>application/json</code></p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "x-api-key",
            "description": "<p>API Key</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>Unique user authentication token</p>"
          }
        ]
      }
    }
  },
  {
    "description": "<p>Returns all app settings. Each setting value is identified by the respective key.</p>",
    "version": "1.0.0",
    "type": "GET",
    "url": "api/v1/settings",
    "title": "Get Settings",
    "group": "Settings",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Accept",
            "description": "<p><code>application/json</code></p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "x-api-key",
            "description": "<p>API Key</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response / HTTP 200 OK",
          "content": "{\n    \"payload\": {\n        \"settings\": [\n            {\n                \"id\": 1,\n                \"created_at\": \"2020-06-17T11:05:27.000000Z\",\n                \"updated_at\": \"2020-06-17T11:05:27.000000Z\",\n                \"key\": \"ABOUT_US\",\n                \"value\": null\n            },\n            {\n                \"id\": 2,\n                \"created_at\": \"2020-06-17T11:05:27.000000Z\",\n                \"updated_at\": \"2020-06-17T11:05:27.000000Z\",\n                \"key\": \"PRIVACY_POLICY\",\n                \"value\": null\n            },\n            {\n                \"id\": 3,\n                \"created_at\": \"2020-06-17T11:05:27.000000Z\",\n                \"updated_at\": \"2020-06-17T11:05:27.000000Z\",\n                \"key\": \"TERMS_AND_CONDITIONS\",\n                \"value\": null\n            }\n        ]\n    },\n    \"message\": \"\",\n    \"result\": true\n}",
          "type": "json"
        }
      ]
    },
    "filename": "/home/chiran/Documents/Emedia/chiran/projects/testApiProject/resources/docs/apidoc/auto_generated/settings.coffee",
    "groupTitle": "Settings",
    "name": "GetApiV1Settings"
  },
  {
    "description": "<p>Returns the value of a single app setting requested by key.</p>",
    "version": "1.0.0",
    "type": "GET",
    "url": "api/v1/settings/{key}",
    "title": "Get Setting",
    "group": "Settings",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "key",
            "description": "<p>Key of the setting</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response / HTTP 200 OK",
          "content": "{\n    \"payload\": {\n        \"id\": 1,\n        \"created_at\": \"2020-06-17T11:05:27.000000Z\",\n        \"updated_at\": \"2020-06-17T11:05:27.000000Z\",\n        \"key\": \"ABOUT_US\",\n        \"value\": null\n    },\n    \"message\": \"\",\n    \"result\": true\n}",
          "type": "json"
        }
      ]
    },
    "filename": "/home/chiran/Documents/Emedia/chiran/projects/testApiProject/resources/docs/apidoc/auto_generated/settings.coffee",
    "groupTitle": "Settings",
    "name": "GetApiV1SettingsKey",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Accept",
            "description": "<p>Set to <code>application/json</code></p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "x-api-key",
            "description": "<p>API Key</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>Unique user authentication token</p>"
          }
        ]
      }
    }
  }
] });
