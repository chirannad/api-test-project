<?php

namespace Database\Seeders;

use App\Entities\Books\Book;
use Illuminate\Database\Seeder;

class BooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Book::factory()->count(50)->create();
    }
}
